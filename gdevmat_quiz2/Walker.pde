class Walker
{
  float xPosition;
  float yPosition;
  float size;
  float rColor;
  float gColor;
  float bColor;

  float rT = 0;
  float gT = 100;
  float bT = 200;
 
 void render()
 {
   float rNoise = noise(rT);
   float gNoise = noise(gT);
   float bNoise = noise(bT);
   
   rColor = map(rNoise, 0, 1, 0, 255);
   gColor = map(gNoise, 0, 1, 0, 255);
   bColor = map(bNoise, 0, 1, 0, 255);
   
   noStroke();
   fill(rColor, gColor, bColor);
   circle(xPosition, yPosition, size);
 
   rT +=0.01;
   gT +=0.01;
   bT +=0.01;
 }
 
  float xT=15;
  float yT=10;
  float sizeT = 30;
 
 void randomWalk()
 {
   float xNoise = noise(xT);
   float yNoise = noise(yT);
   float sizeNoise = noise(sizeT);
   xPosition = map(xNoise, 0, 1, Window.left, Window.right);
   yPosition = map(yNoise, 0, 1, Window.bottom, Window.top);
   size =  map(sizeNoise, 0, 1, 10, 100);
 
   xT+=0.01f;
   yT+=0.01f;
   sizeT +=0.01;
 }
}
