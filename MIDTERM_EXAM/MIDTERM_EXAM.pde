void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  matter = new Walker[100];
  spawnMatter();
}

Walker blackHole;
Walker matter[];

void spawnMatter()
{
  for(int i = 0; i < 100; i++)
  {
    float gauss = randomGaussian();
    float std = 50;
    float meanX = random(Window.left, Window.right);
    float meanY = random(Window.bottom, Window.top);
    float xPosition = std * gauss + meanX;
    float yPosition = std * gauss + meanY;
    
    matter[i] = new Walker(xPosition, yPosition, random(50));
    matter[i].r = map(noise(random(255)), 0, 1, 0, 255);
    matter[i].g = map(noise(random(255)), 0, 1, 0, 255);
    matter[i].b = map(noise(random(255)), 0, 1, 0, 255);
  } 
  blackHole = new Walker(random(Window.left, Window.right), random(Window.bottom, Window.top), 50);
}

void draw()
{
  background(0);
  
  for(int i = 0; i < 100; i++)
  {
    matter[i].render();
    PVector range = PVector.sub(blackHole.position, matter[i].position);
    
    range.normalize().mult(10);
    
    matter[i].position.add(range);
  }
  
  noStroke();
  
  blackHole.render();
  
  if(frameCount >= 200)
  {
    frameCount = 0;
    spawnMatter();
  }
  
  println(frameCount);
}
