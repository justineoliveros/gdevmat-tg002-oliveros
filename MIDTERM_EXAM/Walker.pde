public class Walker
{
   public PVector position;
   public float scale = 50;
   public float r = 255, g = 255, b = 255, a = 255;
   
   Walker()
   {
      position = new PVector(); 
   }
   
   Walker(float xPosition, float yPosition)
   {
      position = new PVector(xPosition, yPosition);
   }
   
   Walker(float xPosition, float yPosition, float scale)
   {
      position = new PVector(xPosition, yPosition);
      this.scale = scale;
   }
   
   Walker(PVector position)
   {
      this.position = position; 
   }
   
   Walker(PVector position, float scale)
   {
      this.position = position; 
      this.scale = scale;
   }
 
   public void render()
   {
      fill(r,g,b,a);
      circle(position.x, position.y, scale); 
   }
}
