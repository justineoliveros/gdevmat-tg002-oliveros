class Splatter
{
  void render()
  {
     float mean = 0;
     float std = 200;
     float gauss = randomGaussian();
  
     float meanScale = 10;
     float stdScale = 50;
     float gaussScale = randomGaussian();
     
     float meanY = 0;
     float stdY = 100;
     float gaussY = randomGaussian();
     
     float xPosition = std * gauss + mean;
     float yPosition = stdY * gaussY + meanY;
     float scale = stdScale * gaussScale + meanScale;
     
     noStroke();
     circle(xPosition, yPosition, scale);
     fill(random(255), random(255), random(255), 30);
 
  }
}
