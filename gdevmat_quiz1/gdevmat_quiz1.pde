void setup()
{
  size(1920, 1080, P3D);
  background(255);
  camera(0, 0, -(height / 2) / tan(PI* 30 / 100), // camera position
  0, 0, 0,  // eye position
  0, -1, 0); // up vector
}

Splatter splat = new Splatter();

void draw()
{  

  if (frameCount == 1000)
  {
    background(255);
  }
  
  else
  {
    splat.render();
  }
  
  println(frameCount);
 
}


  
