class Walker
{
  float xPosition;
  float yPosition;
  
  void render()
  {
    circle(xPosition, yPosition, 15);
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    fill(random(255), random(255), random(255), random(255));
    
    if (decision == 0)
    {
      yPosition+=5;
    }
    
    else if (decision == 1)
    {
      yPosition-=5;
    }
    
    else if (decision == 2)
    {
      xPosition+=5;
    }
    
    else if (decision == 3)
    {
      xPosition-=5;
    }
    
    else if (decision == 4)
    {
      yPosition+=5;
      xPosition+=5;
    }
    
    else if (decision == 5)
    {
      yPosition-=5;
      xPosition-=5;
    }
    
    else if (decision == 6)
    {
      yPosition+=5;
      xPosition-=5;
    }
    
    else if (decision == 7)
    {
      yPosition-=5;
      xPosition+=5;
    }
  }
  
}
