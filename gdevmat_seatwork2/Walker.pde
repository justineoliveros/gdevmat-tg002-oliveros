class Walker
{
  public Vector2 position;
  
  Walker()
  {
    position = new Vector2();
  }

  Walker(Vector2 position)
  {
    this.position = position;
  }
  void render()
  {
    noStroke();
    fill(random(255), random(255), random(255), random(255));
    circle(position.x, position.y, 30);
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    
    if (decision == 0)
    {
      position.y+=5;
    }
    
    else if (decision == 1)
    {
      position.y-=5;
    }
    
    else if (decision == 2)
    {
      position.x+=5;
    }
    
    else if (decision == 3)
    {
      position.x-=5;
    }
    
    else if (decision == 4)
    {
      position.y+=5;
      position.x+=5;
    }
    
    else if (decision == 5)
    {
      position.y-=5;
      position.x-=5;
    }
    
    else if (decision == 6)
    {
      position.y+=5;
      position.x-=5;
    }
    
    else if (decision == 7)
    {
      position.y-=5;
      position.x+=5;
    }
  }
  
}
