void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, // camera position
  0, 0, 0,  // eye position
  0, -1, 0); // up vector
}

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x, y);
}

void draw()
{
  background(0);
  
  Vector2 mouse = mousePos();
  Vector2 handle = mousePos();
  
  mouse.normalize();
  mouse.mult(250);
  
  strokeWeight(15);
  stroke(150, 38, 38, 225);
  line(0, 0, mouse.x, mouse.y);
  
  strokeWeight(5);
  stroke(242, 247, 247, 150);
  line (0, 0, mouse.x, mouse.y);
  
  
  handle.normalize();
  handle.mult(60);
  
  strokeWeight(15);
  stroke(117, 114, 114, 255);
  line(0, 0, handle.x, handle.y);
  
  println("Magnitude: " + mouse.mag());
}
